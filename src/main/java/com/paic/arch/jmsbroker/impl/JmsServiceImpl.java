package com.paic.arch.jmsbroker.impl;

import com.paic.arch.jmsbroker.JmsService;
import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.broker.region.DestinationStatistics;
import org.springframework.beans.factory.InitializingBean;

import static com.paic.arch.jmsbroker.SocketFinder.findNextAvailablePortBetween;

/**
 * @author linyuanxing
 * @version 1.0
 * @since 2018/3/5
 */
public class JmsServiceImpl implements JmsService, InitializingBean {
    public static final String DEFAULT_BROKER_URL_PREFIX = "tcp://localhost:";

    private BrokerService brokerService;

    private String brokerUrl;

    private void startEmbeddedBroker() throws Exception {
        brokerService.start();
    }

    private void createEmbeddedBroker() throws Exception {
        brokerService = new BrokerService();
        brokerService.setPersistent(false);
        brokerService.addConnector(brokerUrl);
    }

    @Override
    public DestinationStatistics getDestinationStatisticsFor(String aDestinationName) throws Exception {
        Broker regionBroker = brokerService.getRegionBroker();
        for (org.apache.activemq.broker.region.Destination destination : regionBroker.getDestinationMap().values()) {
            if (destination.getName().equals(aDestinationName)) {
                return destination.getDestinationStatistics();
            }
        }
        throw new IllegalStateException(String.format("Destination %s does not exist on broker at %s", aDestinationName, brokerUrl));
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        brokerUrl = DEFAULT_BROKER_URL_PREFIX + findNextAvailablePortBetween(41616, 50000);
        createEmbeddedBroker();
        startEmbeddedBroker();
    }

    @Override
    public void stopTheRunningBroker() throws Exception {
        if (brokerService == null) {
            throw new IllegalStateException("Cannot stop the broker from this API: " +
                    "perhaps it was started independently from this utility");
        }
        brokerService.stop();
        brokerService.waitUntilStopped();
    }

    @Override
    public String getBrokerUrl() {
        return brokerUrl;
    }
}
