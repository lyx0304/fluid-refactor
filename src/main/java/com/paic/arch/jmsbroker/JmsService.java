package com.paic.arch.jmsbroker;

import org.apache.activemq.broker.region.DestinationStatistics;

/**
 * @author linyuanxing
 * @version 1.0
 * @since 2018/3/4
 */
public interface JmsService {

    /**
     *
     * @param aDestinationName
     * @return
     * @throws Exception
     */
    DestinationStatistics getDestinationStatisticsFor(String aDestinationName) throws Exception;

    /**
     * 启动服务
     * @throws Exception
     */
    void afterPropertiesSet() throws Exception;

    /**
     * 关闭服务
     * @throws Exception
     */
    void stopTheRunningBroker() throws Exception;

    /**
     * 获取url
     * @return
     */
    String getBrokerUrl();
}
